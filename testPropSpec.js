const propsSPec = {
  'Button': {
    props: [
      {
        label: '内容',
        name: 'text',
        type: 'string'
      },
      {
        label: '按钮类型',
        name: 'type',
        type: 'string',
        will: 'Select',
        willProps: {
          options: [
            {
              label: '默认',
              value: 'default'
            },
            {
              label: '主要',
              value: 'primary'
            },
            {
              label: '链接',
              value: 'link'
            },
            {
              label: '文本',
              value: 'text'
            }
          ]
        }
      },
      {
        label: '按钮大小',
        name: 'size',
        type: 'string',
        will: 'Select',
        willProps: {
          options: [
            {
              label: '大',
              value: 'large'
            },
            {
              label: '中',
              value: 'middle'
            },
            {
              label: '小',
              value: 'small'
            }
          ]
        }
      },
      {
        label: '加载',
        name: 'loading',
        type: 'boolean',
      },
      {
        label: '点击事件',
        name: 'onClick',
        type: 'function',
      }
    ],
    isContainer: true,
    isOverLayer: false
  },
  'Select': {
    props: [
      {
        label: '值',
        name: 'value',
        type: 'string'
      },
      {
        label: '默认文本',
        name: 'placeholder',
        type: 'string'
      },
      {
        label: '配置项',
        name: 'options',
        type: [],
      },
      {
        label: '选择框模式',
        name: 'mode',
        type: 'string',
        will:'Select',
        willProps: {
          options:[
            {
              label:'多选',
              value:'multiple'
            },
            {
              label:'标签',
              value:'tags'
            }
          ]
        }
      },
      {
        label: '选择框大小',
        name: 'size',
        type: 'string',
        will: 'Select',
        willProps: {
          options: [
            {
              label: '大',
              value: 'large'
            },
            {
              label: '中',
              value: 'middle'
            },
            {
              label: '小',
              value: 'small'
            }
          ]
        }
      },
      {
        label: '禁用',
        name: 'disabled',
        type: 'boolean'
      },
      {
        label: '加载',
        name: 'loading',
        type: 'boolean'
      },
      {
        label: '支持清除',
        name: 'allowClear',
        type: 'boolean'
      },
      {
        label: '配置项可搜索',
        name: 'showSearch',
        type: 'boolean'
      },
      {
        label: '根据输入项筛选事件',
        name: 'filterOption',
        type: 'function'
      },
      {
        label: '选项值变化事件',
        name: 'onChange',
        type: 'function'
      }
    ],
    isContainer: true,
    isOverLayer: false
  }

}

const keys = [
  {
    version: '1.0.0',
    key: 'PACKAGE_PROPS_SPEC:dev-ui',
    value: JSON.stringify(propsSPec),
  }
]

window.__httpClient('/api/v1/persona/batchSetValue', { keys })