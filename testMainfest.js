const manifest = {
  'Button': {
    category: '基础组件',
    variants: [
      {
        icon: {
          type: 'platform',
          name: 'smart_button',
          initialProps: {}
        },
        label: '按钮',
        desc: '按钮用于提交、提示、操作',
        initialProps: {
          text: '按钮',
          type: 'primary',
          size: 'middle',
          loading: false
        }
      }
    ]
  },
  'Select': {
    category: '基础组件',
    variants: [
      {
        icon: {
          type: 'platform',
          name: 'smart_button',
          initialProps: {}
        },
        label: '下拉选择器',
        desc: '弹出一个下拉菜单给用户选择操作',
        initialProps: {
          allowClear:false,
          placeholder:'请选择',
          size: 'middle',
          loading: false
        }
      }
    ]
  }
}

const keys = [
  {
    version: '1.0.0',
    key: 'PACKAGE_MANIFEST:dev-ui',
    value: JSON.stringify(manifest),
  }
]

window.__httpClient('/api/v1/persona/batchSetValue', { keys })