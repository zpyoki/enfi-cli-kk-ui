import React from 'react'
import { Button } from 'antd'
// import styled from 'styled-components'

export default function ({ text, type = 'primary', size, loading, onClick }) {
  return (
    <>
      <Button onClick={onClick} type={type} size={size} loading={loading}>{text}</Button>
    </>
  )
}
