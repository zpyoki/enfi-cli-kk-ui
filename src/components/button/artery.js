const manifest = {
  category: '基础组件',
  variants: [
    {
      icon: {
        type: 'platform',
        name: 'smart_button',
        initialProps: {}
      },
      label: '按钮',
      desc: '按钮用于提交、提示、操作',
      initialProps: {
        text: '按钮',
        type: 'primary',
        size: 'middle',
        loading: false
      }
    }
  ]
}

const propsSpec = {
  props: [
    {
      label: '内容',
      name: 'text',
      type: 'string'
    },
    {
      label: '按钮类型',
      name: 'type',
      type: 'string',
      will: 'Select',
      willProps: {
        options: [
          {
            label: '默认',
            value: 'default'
          },
          {
            label: '主要',
            value: 'primary'
          },
          {
            label: '链接',
            value: 'link'
          },
          {
            label: '文本',
            value: 'text'
          }
        ]
      }
    },
    {
      label: '按钮大小',
      name: 'size',
      type: 'string',
      will: 'Select',
      willProps: {
        options: [
          {
            label: '大',
            value: 'large'
          },
          {
            label: '中',
            value: 'middle'
          },
          {
            label: '小',
            value: 'small'
          }
        ]
      }
    },
    {
      label: '加载',
      name: 'loading',
      type: 'boolean',
    },
    {
      label: '点击事件',
      name: 'onClick',
      type: 'function',
    }
  ],
  isContainer: true,
  isOverLayer: false
}

export default {
  key: 'Button',
  manifest,
  propsSpec
}